<?php
header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
header('Content-Type: text/html; charset=utf-8');
?><!DOCTYPE html>
<html lang="oc">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>Verificador : resultats</title>
	<meta http-equiv="pragma" content="no-cache">
	<link rel="icon" href="https://www.mejans.fr/themes/aucel/img/favicon/favicon.png">
	<meta name="theme-color" content="#134E5E">
	<meta property="og:site_name" content="Verficador">
	<meta property="og:description" content="Verificacion de configuracion lingüistica">
	<meta property="og:locale" content="oc">
	<meta property="og:image" content="accept_language_occitan.png">
	<meta property="og:image:width" content="400">
	<meta property="og:image:height" content="400">
	<meta property="og:image:type" content="image/png">
	<style>
#fons {
  background: #134E5E;
}
#cos, #licencia {
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, Arial, sans-serif;
  line-height: 1.6;
  color: #222;
  max-width: 40rem;
  margin: auto;
  background: #fafafa;
}
#cos{
  margin-top: 15px;
  padding: 2rem;
}
img {
  max-width: 100%;
}
a {
  color: #134E5E;
}
h1, h2, strong {
  color: #111;
}
.amagat {
  display: none;
}
.cadron {
  border-top: 3px solid #134E5E;
  border-bottom: 3px solid #134E5E;
}
#licencia{
  height: 30px;
  background-color: rgba(0,0,0,0.5);
  color:white;
  margin-bottom: 15px;
  padding: 1rem 2rem;
}
#licencia a, #licencia strong{
  color:white;
  text-decoration:none;
}
.bon{
  color:#71B280;
  font-size: large;
}
.marrit{
  color:red;
  font-size: large;
}
	</style>
	<script>
	function deteccion(){
			var lang = navigator.languages;
			if (lang.indexOf('oc') !== -1) {
				console.log(lang);
				if (lang[0] == 'oc') {
					console.log('L’occitan es difinit coma primièra lenga');
					document.getElementById("bon").style.display = "block";
				}else{
        		document.getElementById("mejan").style.display = "block";
				document.getElementById("fons").style.backgroundColor = "#43135E";
				document.getElementById("cadron").style.borderTop = "3px solid #43135E";
                document.getElementById("cadron").style.borderBottom = "3px solid #43135E"  				}
			}else{
				document.getElementById("marrit").style.display = "block";
				document.getElementById("fons").style.backgroundColor = "#912D3F";
				document.getElementById("cadron").style.borderTop = "3px solid #912D3F";
                document.getElementById("cadron").style.borderBottom = "3px solid #912D3F"}
			    document.getElementById('preflg').textContent = lang;
	        };
		</script>
  </head>
  <body onload="deteccion()" id="fons">
  <div id="cos">
    <h1>Verificador</h1>
    <h2>Promocion de l’occitan en linha</h2>
	<p>Cada còp que visitatz un site web vòstre navegador envia una lista de paramètres. Un d’aqueles es vòstra preferéncia lingüistica. Aqueste site vos permet de verificar se vòstre navegador es configurat per demandar la lenga occitana.</p>
    <p><strong>Consultar los resultats de vòstre navegador</strong></p>
    <hr>
	<h3>Pròva costat servidor</h3>
    <p><?php
	if (isset($_SERVER["HTTP_ACCEPT_LANGUAGE"])){

		$x = explode(",",$_SERVER["HTTP_ACCEPT_LANGUAGE"]);
        foreach ($x as $val) {
        #check for q-value and create associative array. No q-value means 1 by rule
            if(preg_match("/(.*);q=([0-1]{0,1}.\d{0,4})/i",$val,$matches))
                $lang[$matches[1]] = (float)$matches[2];
            else
                $lang[$val] = 1.0;}

		if ((isset($lang[oc])) AND ($lang[oc]==1)){
			echo '<span class="bon">&#x2714;&#xFE0F;</span> Vòstre navegador indica que vòstra lenga preferida es l’occitan.';
		}elseif (($lang[oc]) AND ($lang[oc] != 1)){
			echo 'Vòstre navegador indica que podètz legir l’occitan mas lo demanda pas coma lenga primièra.';
		}else{
			echo '<span class="marrit">&#x2716;&#xFE0F;</span> Vòstre navegador indica pas l’occitan dins vòstra preferéncias lingüisticas.';
  }else{
    echo 'Cap de donada pas enviada';}

?></p>
	<h3>Pròva costat client</h3>
	<noscript>Aquesta pròva requerís l’activacion del JavaScript</noscript>
		<p id="complementari">
		<span id="bon" class="amagat"><span class="bon">&#x2714;&#xFE0F;</span> Lo JavaScript declara que demandatz l’occitan coma primièra lenga. Podètz visitar de sites coma que se botaràn solets en occitan.</span>
		<span id="mejan" class="amagat">Lo JavaScript declara que demandatz l’occitan mas pas coma lenga preferida en primièr. Unes sites utilizaràn pas que la primièra lenga e en cas non disponibilitat, se botaràn sovent en anglés.</span>
		<span id="marrit" class="amagat"><span class="marrit">&#x2716;&#xFE0F;</span> Lo JavaScript declara PAS que demandatz l’occitan</span>
		<div id="cadron" class="cadron">Vòstras preferéncias lingüisticas : <span id="preflg"></span></div>
		</p>
    <h2>Parametratge</h2>
    <p>Segon lo logicial qu’emplegatz los menús varian.</p>
	<h3>Firefox</h3>
	<span>Dintratz <strong>about:preferences</strong> dins la barra d’adreça del navegador puèi quichatz Entrada.<br>
	Cercatz la partida « Lengas ».<br>
	Clicatz lo boton « Causir… » per far veire la lista de lengas.<br>
	Dins la novèla fenèstra vos cal far montar l’occitan ennaut, se cal ajustatz-lo amb la lista enbàs.<br>
	Enregistratz en validant.<br><br>
	Amb Firefox mobil vos cal dintrar <strong>about:config</strong> e botar dins la barra de recèrca « Lang ». Cambiatz la valor per que siá de la forma « oc,fr-FR,es,en » segon las lengas que sabètz legir.</span>
	<h3>Chrome / Microsoft Edge / Chronium</h3>
	<span>Dintratz <strong>chrome://settings/languages</strong> dins la barra d’adreça.<br>
	Desplegatz lo menú Lenga.<br>
	Clicatz lo boton per ajustar una lenga e ajustatz l’occitan.<br>
	Clicatz a man drecha de l’occitan e causissètz « Desplaçar ennaut » per lo plaçar en primièra posicion.</span>
	<h3>Internet Explorer</h3>
	<span>Anatz a las opcions Internet.<br>
	A l’onglet General, dins la seccion Aparéncia clicatz lo boton Lengas.<br>
	Clicatz lo boton per ajustar una lenga e ajustatz l’occitan en validant amb OK.<br>
	Clicatz a man drecha de l’occitan e causissètz « Desplaçar ennaut » per lo plaçar en primièra posicion.</span>
  </div>
<div id="licencia"><a href="https://www.mejans.fr/pagina/a-prepaus.html#licencia">Licéncia <strong>CC-BY-NC-SA</strong></a>
	</div>
  </body>
</html>
